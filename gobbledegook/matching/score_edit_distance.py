from editdistance import eval as get_edit_distance

def score_edit_distance(s1, s2, case_sensitive=True):
	if s1 is None or s2 is None:
		return 0
	else:
		if not case_sensitive:
			s1 = str(s1).lower()
			s2 = str(s2).lower()
		else:
			s1 = str(s1)
			s2 = str(s2)
		edit_distance = get_edit_distance(s1, s2)
		max_length = max(len(s1), len(s2))
		if max_length==0:
			return 1
		distance_to_length_ratio = edit_distance/max_length
		non_negative_score = max(0, 1-distance_to_length_ratio)
		return non_negative_score

def score_edit_distance_and_initial(s1, s2, edit_weight=1, initial_weight=1, case_sensitive=False):
	if s1 == s2:  return 1
	try:
		if not case_sensitive: s1, s2 = s1.lower(), s2.lower()
		edit_score = score_edit_distance(s1=s1, s2=s2)
		if len(s1)>0 and len(s2)>0:
			initial_score = int(s1[0]==s2[0])
		else:
			initial_score = 0

		return (edit_score*edit_weight + initial_score*initial_weight)/(edit_weight+initial_weight)
	except:
		return 0