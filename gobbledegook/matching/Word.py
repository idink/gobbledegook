
import re
from .score_edit_distance import score_edit_distance_and_initial


class Word:
	def __init__(self, string, distance_func=None):
		if string is None: string = ''
		if type(string) is Word:
			string = string._string
		else:
			string = str(string)
		self._string = re.sub(r'\W+', '', string)

		if distance_func is None:
			distance_func = score_edit_distance_and_initial
		self._score_distance = distance_func

	@property
	def string(self):
		return self._string

	@property
	def initial(self):
		return self.string[0].upper()

	@property
	def length(self):
		return len(self._string)

	def __sub__(self, other):
		"""
		:type other: Word
		:rtype: float
		"""

		try:
			result = 1 - self._score_distance(self.string, str(other))
		except:
			print(f'{self}, {other}')
			raise
		return result

	def get_similarity(self, other):
		if other is None: return 0
		return max(0, min(1, self._score_distance(self.string, str(other))))

	def __eq__(self, other):
		return self.string == str(other)

	def lower(self, inplace=False):
		if inplace:
			self._string = self.string.lower()
		else:
			return self.__class__(string=self.string.lower(), distance_func=self._score_distance)

	def __repr__(self):
		return self._string

	def __str__(self):
		return self._string

