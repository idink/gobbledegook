from slytherin.progress import ProgressBar
from .score_sentence_distance import score_sentence_distance



def _find_closest_match(string, candidates, min_score=0, score_func=score_sentence_distance, **kwargs):
	best_match = None
	best_match_index = None
	highest_score = min_score
	best_score = None

	for index, candidate in enumerate(candidates):
		try:
			current_score = score_func(string, candidate, **kwargs)
		except:
			current_score = 0

		if current_score>highest_score:
			best_match = candidate
			best_match_index = index
			highest_score = current_score
			best_score = highest_score

	return {'string':string, 'match': best_match, 'index': best_match_index, 'score': best_score}

def find_closest_match(list_of_strings, candidates, min_score=0, show_progress=True, score_func=score_sentence_distance, **kwargs):
	if show_progress:
		matches = list(ProgressBar.map(
			function=lambda x:_find_closest_match(string=x, candidates=candidates, min_score=min_score, score_func=score_func, **kwargs),
			iterable=list_of_strings,
			text='finding closest match'
		))
	else:
		matches = list(map(
			func=lambda x:_find_closest_match(string=x, candidates=candidates, min_score=min_score, score_func=score_func, **kwargs),
			iter1=list_of_strings
		))

	if show_progress: print('done!')
	return matches