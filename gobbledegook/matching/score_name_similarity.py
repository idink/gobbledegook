import re
import itertools
from .score_edit_distance import score_edit_distance

def position_similarity(l1, l2, distance_penalty_function=None, similarity_score_func=None, leftward_weight = 1.1):
	"""
	finds the similarity between two lists prioritizing the distance between position of similar items
	:type l1: list
	:type l2: list
	:return:
	"""
	if distance_penalty_function is None:
		distance_penalty_function = lambda n: 1 + n/(min(len(l1), len(l2)))
	if similarity_score_func is None:
		similarity_score_func = lambda x, y: (x == y)*1

	items_and_positions1 = zip(l1, range(len(l1)))
	items_and_positions2 = zip(l2, range(len(l2)))
	cross = itertools.product(items_and_positions1, items_and_positions2)
	cross_scored = [(left_tup, right_tup, similarity_score_func(left_tup[0], right_tup[0]), abs(left_tup[1] - right_tup[1])) for left_tup, right_tup in cross]
	cross_scored.sort(
		key=lambda x: (-x[2], x[3], x[0][1], x[1][1])
	)  # sort by: highest equality score, lowest position distance, positions of left and right

	# pick unique items
	#pairs = []
	total_score = 0
	total_len = len(l1)+len(l2)
	total_weight = 0
	while(len(cross_scored)>0):
		first_pair = cross_scored.pop(0)
		left_item, right_item, score, distance = first_pair
		cross_scored = [x for x in cross_scored if x[0]!=left_item and x[1]!=right_item]
		average_position = (left_item[1]+right_item[1])/total_len
		weight = 1/(leftward_weight**average_position)
		total_weight += weight
		total_score += score*weight/distance_penalty_function(distance)
		"""
		print(
			f"score:{score}, average_position:{average_position}, distance_penalty_function(distance):{distance_penalty_function(distance)}, weight:{weight}, total_weight:{total_weight}, total_score:{total_score}"
		)
		"""
		#pairs.append(first_pair)
	normal_score = total_score/total_weight*(min(len(l1), len(l2))/max(len(l1), len(l2)))
	return normal_score


class WordPosition:
	def __init__(self, word, position, length):
		self.word = word
		self.position = position
		self.length = length

	def get_position_weight(self, sensitivity=0.5):
		return 1/(1+ self.position*sensitivity)

	def __repr__(self):
		return f'{self.word},{self.position}'

class WordPair:
	def __init__(self, wordpos1, wordpos2):
		"""
		:type wordpos1: WordPosition
		:type wordpos2: WordPosition
		"""
		self.wordpos1 = wordpos1
		self.wordpos2 = wordpos2
		self.score = None



	@staticmethod
	def from_tuple(tup, length1, length2):
		tup1, tup2 = tup
		word1, position1 = tup1
		word2, position2 = tup2

		wordpos1 = WordPosition(word=word1, position=position1, length=length1)
		wordpos2 = WordPosition(word=word2, position=position2, length=length2)
		word_pair = WordPair(wordpos1=wordpos1, wordpos2=wordpos2)
		return word_pair

	def get_score(self, case_sensitivity=0.1, initial_score=0.5, position_sensitivity=0.5):
		if self.score is not None:
			return self.score
		if self.wordpos1.word == self.wordpos2.word:
			raw_score = 1
		elif self.wordpos1.word.lower() == self.wordpos2.word.lower():
			raw_score = 1 - case_sensitivity
		elif self.wordpos1.word[0] == self.wordpos2.word[0]:
			raw_score = initial_score
		elif self.wordpos1.word[0].lower() == self.wordpos2.word[0].lower():
			raw_score = max(0, initial_score - case_sensitivity)
		else:
			raw_score = 0

		if raw_score< 1-case_sensitivity:
			edit_score = score_edit_distance(s1=self.wordpos1.word, s2=self.wordpos2.word)
			raw_score = raw_score*edit_score

		distance = abs(self.wordpos1.position - self.wordpos2.position)

		distance_weight1 = 1 - distance*position_sensitivity / (self.wordpos1.length + 1)
		distance_weight2 = 1 - distance*position_sensitivity / (self.wordpos2.length + 1)
		position_weight1 = self.wordpos1.get_position_weight(sensitivity=position_sensitivity)
		position_weight2 = self.wordpos2.get_position_weight(sensitivity=position_sensitivity)
		self.score = raw_score * (distance_weight1 * distance_weight2 * position_weight1 * position_weight2)**0.5
		return self.score

	def __repr__(self):
		return f'{str(self.wordpos1)} {str(self.wordpos2)} {self.get_score()}'


def score_name_similarity(name1, name2, case_sensitivity = 0.01, initial_score = 0.33, position_sensitivity=0.2):
	"""
	:type name1: str
	:type name2: str
	:type case_sensitivity: float
	:rtype: float
	"""
	name1 = str(name1)
	name2 = str(name2)



	# word by word
	words1 = re.findall(r'[^\s!,.?":;]+', name1)
	words2 = re.findall(r'[^\s!,.?":;]+', name2)
	words1 = list(zip(words1, range(len(words1))))
	words2 = list(zip(words2, range(len(words2))))

	cross = [WordPair.from_tuple(x, length1=len(words1), length2=len(words2)) for x in itertools.product(words1, words2)]

	cross.sort(
		key=lambda x:x.get_score(
			case_sensitivity=case_sensitivity,
			initial_score=initial_score,
			position_sensitivity=position_sensitivity
		),
		reverse=True
	)
	best_scores = []
	while len(cross)>0:
		first_word_pair = cross.pop(0)
		best_scores.append(first_word_pair)
		# remove the pair from cross list
		word1 = first_word_pair.wordpos1.word
		word2 = first_word_pair.wordpos2.word
		cross = [word_pair for word_pair in cross if word_pair.wordpos1.word!=word1 and word_pair.wordpos2.word!=word2]

	total_score = sum([good_score.score for good_score in best_scores])
	average_len = (len(words1) + len(words2))/2

	return total_score/average_len



