from .score_edit_distance import score_edit_distance

def score_weighted_edit_distance(s1, s2, distance_weight=1, first_letter_weight=1, common_letters_weight=1):
	distance_score = score_edit_distance(s1, s2)
	first_letter_score = float(s1[0] == s2[0])
	num_common_letters = len(set(s1).intersection(s2))
	common_letters_score = num_common_letters/min(len(s1), len(s2))
	total_score = distance_score * distance_weight + first_letter_score * first_letter_weight + common_letters_score * common_letters_weight
	total_weight = distance_weight + first_letter_weight + common_letters_weight

	return total_score/total_weight
