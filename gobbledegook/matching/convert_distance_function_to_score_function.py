from editdistance import eval as get_edit_distance

def convert_distance_function_to_score_function(distance_func=get_edit_distance):
	def score_func(s1, s2):
		distance = get_edit_distance(s1, s2)
		score = max(0,1-float(distance)/max(len(s1), len(s2)))
		return score
	return score_func