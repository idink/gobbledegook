
from itertools import product
from ..string import SentenceCompressor
from .score_edit_distance import score_edit_distance
from .score_weighted_edit_distance import score_weighted_edit_distance



def score_sentence_distance1(s1, s2, score_func = score_edit_distance):
	words1 = s1.split()
	words2 = s2.split()
	tuples = list(product(words1, words2))
	distances = list(map(lambda x: (x[0], x[1], score_func(x[0], x[1])), tuples))
	distances.sort(key=lambda x: -x[2])
	result = []
	while (len(distances) > 0):
		best_match = distances[0]
		result.append(best_match)
		distances = [x for x in distances if x[0] != best_match[0] and x[1] != best_match[1]]
	return result

def score_sentence_distance(s1, s2, score_func=score_weighted_edit_distance, **kwargs):
	s1 = s1.lower()
	s2 = s2.lower()
	sc = SentenceCompressor()
	compressed1 = sc.compress(s1)
	compressed2 = sc.compress(s2)

	return score_func(compressed1, compressed2, **kwargs)