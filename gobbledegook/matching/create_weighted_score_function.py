from .score_edit_distance import score_edit_distance

def create_weighted_score_function(score_func=score_edit_distance, first_letter_weight=1):
	def new_score_func(s1, s2):
		original_result = score_func(s1, s2)
		first_letter_result = float(s1[0]==s2[0])
		return (original_result+first_letter_result*first_letter_weight)/(1.0+first_letter_weight)
	return new_score_func