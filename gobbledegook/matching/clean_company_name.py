from cleanco import cleanco

def clean_company_name(s):
	try:
		return cleanco(s).clean_name()
	except:
		return s