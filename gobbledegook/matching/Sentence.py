from itertools import zip_longest
import re
from .Word import Word

class Sentence:
	def __init__(self, string, distance_func=None):
		self._string = str(string)
		self._distance_func = distance_func
		words = [Word(string=s, distance_func=self._distance_func) for s in re.findall(r'[^\s!,.?":;]+', self._string)]
		self._words = [word for word in words if word.length>0]

	def __sub__(self, other):
		"""
		:type other: Sentence
		:rtype: float
		"""

		word_pairs = zip_longest(self._words, other._words)
		distances = [word1-word2 for word1, word2 in word_pairs]
		up_to_3 = min(3, len(distances))
		important_distances = distances[:up_to_3]
		average = sum(important_distances)/up_to_3
		return average

	def get_similarity(self, other):
		if not isinstance(other, Sentence):
			other = Sentence(other)
		word_pairs = zip_longest(self._words, other._words)
		similarities = [word1.get_similarity(word2) if word1 is not None else 0 for word1, word2 in word_pairs]
		up_to_3 = min(3, len(similarities))
		important_similarities = similarities[:up_to_3]
		average = sum(important_similarities) / up_to_3
		return average

	def __contains__(self, item):
		"""
		:type item: Word or str
		:rtype: bool
		"""
		return str(item) in self._words

	def get_initials(self):
		return [word.initial for word in self._words]

	def get_initial_intersection_score(self, other):
		"""
		:type other: Sentence
		:rtype: int
		"""
		return len(set(self.get_initials()).intersection(set(other.get_initials())))

	@property
	def words(self):
		return self._words

	def get_word(self, i=1):
		try:
			return self.words[i-1].string
		except:
			return None

	@property
	def length(self):
		return len(self.words)

	def lower(self, inplace=False):
		if inplace:
			self._words = [w.lower(inplace=False) for w in self.words]
			self._string = self._string.lower()
		else:
			return self.__class__(string=self._string.lower(), distance_func=self._distance_func)

	def remove_words(self, words, inplace=False):
		"""
		:type words: list of Word or list of str
		"""
		new_words = [w for w in self.words if w not in words]
		new_strings = [w.string for w in new_words]
		if inplace:
			self._words = new_words
			self._string = ' '.join(new_strings)
		else:
			return self.__class__(string=' '.join(new_strings), distance_func=self._distance_func)


	def replace_words(self, replacement_dict, inplace=False):
		"""
		:param replacement_dict: a dictionary with words to be replaced as keys and replacements as values
		:type replacement_dict: dict
		"""
		new_strings = [replacement_dict[w.string] if w.string in replacement_dict else w.string for w in self.words]
		if inplace:
			self._words = [Word(string) for string in new_strings]
			self._string = ' '.join(new_strings)
		else:
			return self.__class__(string=' '.join(new_strings), distance_func=self._distance_func)


	def __repr__(self):
		return ' '.join([str(word) for  word in self._words])