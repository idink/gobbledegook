from slytherin.progress import ProgressBar

def _fast_remove_words_from_sentence(sentence, words, case_sensitive=False):
	try:
		if case_sensitive:
			result = ' '.join([word for word in sentence.split() if word not in words])
		else:
			result = ' '.join([word for word in sentence.split() if word.lower() not in words])
	except:
		result = sentence
	return result

def remove_words_from_sentence(sentence, words, case_sensitive=False):
	if not case_sensitive:
		words = [word.lower() for word in words]
	return _fast_remove_words_from_sentence(sentence=sentence, words=words, case_sensitive=case_sensitive)

def remove_words_from_sentences(sentences, words, case_sensitive=False, show_progress=True):
	if not case_sensitive:
		words = [word.lower() for word in words]
	if show_progress:
		result = list(ProgressBar.map(
			function=lambda x: _fast_remove_words_from_sentence(sentence=x, words=words, case_sensitive=case_sensitive),
			iterable=sentences, text='removing words'
		))
		print('done!')
	else:
		result = list(map(
			lambda x: _fast_remove_words_from_sentence(sentence=x, words=words, case_sensitive=case_sensitive),
			sentences
		))
	return result
