from re import sub
from slytherin.progress import ProgressBar


def _replace_regexes(string, regexes, replacement=''):
	result = string
	for regex in regexes:
		result = sub(pattern=regex, string=result, repl=replacement)
	return result

def _replace_regex(string, regex, replacement=''):
	try:
		return sub(pattern=regex, string=string, repl=replacement)
	except:
		return string

def replace_regex(string, regex, replacement='', show_progress=True):
	if type(string) is str and type(regex) is str:
		return sub(pattern=regex, repl=replacement, string=string)
	elif type(string) is str: # but regex is a list
		return _replace_regexes(string=string, regexes=regex, replacement=replacement)
	elif type(regex) is str: # but string is a list
		if show_progress:
			return list(ProgressBar.map(
				function=lambda x: _replace_regex(string=x, regex=regex, replacement=replacement),
				iterable=string,
				text='replacing regex'
			))
		else:
			return list(map(
				lambda x: _replace_regex(string=x, regex=regex, replacement=replacement),
				string
			))
	else: # both string and regex are lists
		if show_progress:
			return list(ProgressBar.map(
				function=lambda x: _replace_regexes(string=x, regexes=regex, replacement=replacement),
				iterable=string,
				text='replacing regexes'
			))
		else:
			return list(map(
				lambda x: _replace_regexes(string=x, regexes=regex, replacement=replacement),
				string
			))
