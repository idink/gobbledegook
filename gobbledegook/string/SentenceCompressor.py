from . import remove_punctuations
from re import sub
from string import printable

class SentenceCompressor:
	def __init__(self):
		self._word_dict = []

	def compress(self, string):
		string = remove_punctuations(string=string, replacement=' ')
		string = sub(pattern=' +', repl=' ', string=string)
		words = string.split(sep=' ')
		new_words = [word for word in set(words) if word not in self._word_dict]
		self._word_dict += new_words

		letters = [printable[self._word_dict.index(word)] for word in words]
		#print(''.join(letters))
		return ''.join(letters)

	def expand(self, string):
		words = [self._word_dict[printable.index(character)] for character in string]
		return ' '.join(words)

