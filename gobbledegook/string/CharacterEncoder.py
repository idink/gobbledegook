import numpy as np

class CharacterEncoder:
	def __init__(self, characters=None):
		self._characters = characters or 'abcdefghijklmnopqrstuvwxyz-,;!?:\'\\|_@#$%ˆ&*˜‘+-=<>()[]{} '
		self._char_to_index = {char:i for i, char in enumerate(self._characters)}
		self._index_to_char = {i:char for i, char in enumerate(self._characters)}

	def get_index(self, character):
		return self._char_to_index.get(character, 'unknown')

	def get_character(self, index):
		return self._index_to_char.get(index, 'unknown')

	@property
	def characters(self):
		return self._characters

	@property
	def length(self):
		return len(self._characters)

	def encode(self, text, max_length):
		"""
		:type text: str
		:type max_length: int
		:rtype: np.ndarray
		"""
		text = text.lower().strip()
		encoded = np.zeros((self.length, max_length), dtype=np.int64)
		for i, character in enumerate(text[:max_length]):
			index = self.get_index(character)
			if index is not 'unknown':
				encoded[index, i] = 1
		return encoded


DEFAULT_DICTIONARY = CharacterEncoder()
def char_to_index(x):
	return DEFAULT_DICTIONARY.get_index(character=x)

def index_to_char(x):
	return DEFAULT_DICTIONARY.get_character(index=x)



