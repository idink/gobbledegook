from re import sub
from string import punctuation as _punctuations
from slytherin.progress import ProgressBar
_special_punctuations = [".", "'"]
_other_punctuations = [x for x in _punctuations if x not in _special_punctuations]

def remove_punctuations_for_one_string(
		string, punc_action=None, replacement=' ', special_punctuations_replacement=''
):
	# remove punctuations except dot '.'
	punc_action = punc_action or str.maketrans({key: replacement for key in _other_punctuations})
	try:
		result = string.translate(punc_action)
		for punctuation in _special_punctuations:
			result = result.replace(punctuation, special_punctuations_replacement)
		result = result.translate(punc_action)
		result = sub(pattern=' +', repl=' ', string=result)

		result = result.strip()
	except:
		result = string
	return result

def remove_punctuations(
		string, punc_action=None, replacement=' ', special_punctuations_replacement='', show_progress=True
):
	# remove punctuations except dot '.'
	punc_action = punc_action or str.maketrans({key: replacement for key in _other_punctuations})

	if type(string) is str:
		result = remove_punctuations_for_one_string(
			string=string, punc_action=punc_action, special_punctuations_replacement=special_punctuations_replacement
		)
	else: # string is a list of strings
		strings = string

		if show_progress:
			result = list(ProgressBar.map(
				function=lambda x: remove_punctuations_for_one_string(
					string=x, punc_action=punc_action,
					special_punctuations_replacement=special_punctuations_replacement
				),
				iterable=strings,
				text='removing punctuations'
			))
		else:
			result = list(map(
				lambda x: remove_punctuations_for_one_string(
					string=x, punc_action=punc_action,
					special_punctuations_replacement=special_punctuations_replacement
				),
				strings
			))


	return result

