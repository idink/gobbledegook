def get_initials(string):
    """
    Converts a string to its initials
    """
    parts = string.split()
    initials = [part[0].upper() for part in parts]
    return initials