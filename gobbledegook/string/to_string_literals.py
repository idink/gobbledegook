from binascii import unhexlify

def bytes_to_string_literals(x, ignore_error=True, print_error=True):
	try:
		return bytes(f"\\u{x}", 'utf-8').decode('unicode-escape')
	except:
		if ignore_error:
			if print_error:
				print(f'Error in bytes_to_string("{x}")')
			return x
		else:
			raise


def hex_to_string_literals(x, ignore_error=True, print_error=True):
	try:
		return unhexlify(x).decode('utf-8')
	except:
		if ignore_error:
			if print_error:
				print(f'Error in hex_to_string_literals("{x}")')
			return x
		else:
			raise