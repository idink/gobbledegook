from slytherin.progress import ProgressBar
import numpy as np

class WordVector:
	def __init__(self, index, word, vector):
		self._index = index
		self._word = word
		self._vector = vector

	@property
	def index(self):
		"""
		:rtype: int
		"""
		return self._index

	@property
	def word(self):
		"""
		:rtype: str
		"""
		return self._word

	@property
	def vector(self):
		"""
		:rtype: np.array
		"""
		return self._vector

	@property
	def size(self):
		"""
		:rtype: int
		"""
		return len(self.vector)

	def __repr__(self):
		return f'{self.index} {self.word} {self.vector}'

class WordVectors:
	def __init__(self, path='c:/code/idin/data/glove.6B.100d.txt', echo=1):
		self._word_to_vectors = {}
		self._index_to_vectors = []
		with open(path, encoding='utf8') as fp:
			lines = fp.readlines()
			pb = ProgressBar(total=len(lines))
			index = 0
			for index, line in enumerate(lines):
				if index / 10000 == index // 10000 and echo: pb.show(amount=index, text=f'WV {index} word vectors')
				line = line.split(" ")
				word = line[0]
				vector = np.array([float(x) for x in line[1:]])
				word_vector = WordVector(index=index, word=word, vector=vector)
				self._word_to_vectors[word] = word_vector
				self._index_to_vectors.append(word_vector)
			if echo: pb.show(amount=index + 1, text=f'WV {index + 1} word vectors')
		self._size = self._index_to_vectors[0].size

	@property
	def size(self):
		"""
		:rtype: int
		"""
		return self._size

	def __getitem__(self, item):
		"""
		:type item: int or str
		:rtype: WordVector
		"""
		if isinstance(item, str):
			return self._word_to_vectors[item]
		elif isinstance(item, int):
			return self._index_to_vectors[item]

	@property
	def words(self):
		return self._word_to_vectors.keys()

	def __contains__(self, item):
		return item in self._word_to_vectors




