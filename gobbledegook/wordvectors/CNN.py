import os
import sys
import time
import pickle
import numpy as np

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
from torch.utils.data import DataLoader, TensorDataset

from sklearn.model_selection import KFold

from .WordEncoder import WordEncoder

class CNN(nn.Module):
	def __init__(
			self, word_encoder, mode='nonstatic', kernel_sizes=None, num_filters=100,
			convolution_method='in_channel__is_embedding_dim', device='cpu'

	):
		"""
		:type word_encoder: WordEncoder
		:param mode:
		:param kernel_sizes:
		:param num_filters:
		:param embedding_dim:
		"""
		super(CNN, self).__init__()
		self._convolution_method = convolution_method
		self._use_cuda = device=='cuda'
		self.kernel_sizes = kernel_sizes or [3, 4, 5]
		self.embedding = word_encoder.get_embedding()
		self.embedding.weight.requires_grad = mode == "nonstatic"
		if self._use_cuda:
			self.embedding = self.embedding.cuda()
		embedding_dim = self.embedding.embedding_dim

		conv_blocks = []
		sentence_len = word_encoder.max_sequence_length_without_start_end
		for kernel_size in self.kernel_sizes:
			# maxpool kernel_size must <= sentence_len - kernel_size+1, otherwise, it could output empty
			maxpool_kernel_size = sentence_len - kernel_size + 1

			if self._convolution_method == "in_channel__is_embedding_dim":
				conv1d = nn.Conv1d(
					in_channels=embedding_dim, out_channels=num_filters, kernel_size=kernel_size,
					stride=1
				)
			else:
				conv1d = nn.Conv1d(
					in_channels=1, out_channels=num_filters, kernel_size=kernel_size * embedding_dim,
					stride=embedding_dim
				)

			component = nn.Sequential(
				conv1d,
				nn.ReLU(),
				nn.MaxPool1d(kernel_size=maxpool_kernel_size)
			)
			if self._use_cuda:
				component = component.cuda()

			conv_blocks.append(component)

		self.conv_blocks = nn.ModuleList(conv_blocks)  # ModuleList is needed for registering parameters in conv_blocks
		self.fc = nn.Linear(num_filters * len(kernel_sizes), 1)

	def forward(self, x):  # x: (batch, sentence_len)
		x = self.embedding(x)  # embedded x: (batch, sentence_len, embedding_dim)

		if self._convolution_method == "in_channel__is_embedding_dim":
			#    input:  (batch, in_channel=1, in_length=sentence_len*embedding_dim),
			#    output: (batch, out_channel=num_filters, out_length=sentence_len-...)
			x = x.transpose(1, 2)  # needs to convert x to (batch, embedding_dim, sentence_len)
		else:
			#    input:  (batch, in_channel=embedding_dim, in_length=sentence_len),
			#    output: (batch, out_channel=num_filters, out_length=sentence_len-...)
			x = x.view(x.size(0), 1, -1)  # needs to convert x to (batch, 1, sentence_len*embedding_dim)

		x_list = [conv_block(x) for conv_block in self.conv_blocks]
		out = torch.cat(x_list, 2)
		out = out.view(out.size(0), -1)
		feature_extracted = out
		out = F.dropout(out, p=0.5, training=self.training)
		return self.fc(out)
		#return F.softmax(self.fc(out), dim=1), feature_extracted

def evaluate(model, x_test, y_test):
	inputs = Variable(x_test)
	preds, vector = model(inputs)
	preds = torch.max(preds, 1)[1]
	if model._use_cuda:
		preds = preds.cuda()
	# eval_acc = sum(preds.data == y_test) / len(y_test)          # pytorch 0.3
	rmse = ((preds.data - y_test)**2).mean()**(0.5)
	#eval_acc = (preds.data == y_test).sum().item() / len(y_test)  # pytorch 0.4
	return rmse, vector.cpu().data.numpy()

