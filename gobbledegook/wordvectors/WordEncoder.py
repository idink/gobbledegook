from pandas import Series
import numpy as np
import torch
import torch.nn as nn
from copy import deepcopy

from slytherin.progress import ProgressBar

from .Vocabulary import Vocabulary, START_TOKEN, END_TOKEN, UNKNOWN_TOKEN, MASK_TOKEN
from .WordVectors import WordVectors
from ..string import shorten_string



class WordEncoder:
	def __init__(
			self, texts, word_vectors=None, use_unknowns=True, unknown_token=UNKNOWN_TOKEN, use_mask=True, mask_token=MASK_TOKEN,
			use_start_end=True, start_token=START_TOKEN, end_token=END_TOKEN, echo=1,
			case_sensitive=False, remove_punctuation=True
	):
		"""
		:type texts: Series
		"""
		echo = max(0, echo)
		self._vocabulary = Vocabulary(
			use_unknowns=use_unknowns, use_start_end=use_start_end, use_mask=use_mask,
			start_token=start_token, end_token=end_token, unknown_token=unknown_token, mask_token=mask_token,
			case_sensitive=case_sensitive, remove_punctuation=remove_punctuation
		)
		self._max_sequence_length_without_start_end = 0
		bar = ProgressBar(total=len(texts)); progress = -1
		num_errors = 0
		error_example = ''
		for progress, text in enumerate(texts):
			if progress/1000==progress//1000 and echo:
				bar.show(amount=progress, text=f'WE building vocabulary: "{shorten_string(s=text, max_length=20)}"')
			try:
				text = self._vocabulary.clean_string(text)
				split_text = text.split(' ')
				self._vocabulary.add_many(split_text)
				if len(split_text)>self._max_sequence_length_without_start_end:
					self._max_sequence_length_without_start_end=len(split_text)
			except Exception as e:
				num_errors += 1
				error_example = e
		self._vocabulary.freeze(use_unknowns=use_unknowns)
		if echo:
			if num_errors>0:
				bar.show(
					amount=progress + 1,
					text=f'WE vocabulary built! number of errors:{num_errors}, example:{error_example}'
				)
			else:
				bar.show(amount=progress + 1, text=f'WE vocabulary built!')

		self._word_vectors = word_vectors or WordVectors()
		weights_matrix = np.zeros((self._vocabulary.size, self._word_vectors.size))
		words_found = 0
		bar = ProgressBar(total=self._vocabulary.size)
		progress = 0

		for word, i in self._vocabulary._mapping.items():
			if echo and progress/100 == progress//1000:
				bar.show(amount=progress, text=f'WE setting weights: {words_found} words found')
			try:
				weights_matrix[i] = self._word_vectors[word].vector
				words_found += 1
			except KeyError:
				weights_matrix[i] = np.random.normal(scale=0.6, size=(self._word_vectors.size,))
			progress+=1
		self._weights_matrix = weights_matrix
		if echo: bar.show(amount=progress, text=f'WE weights set with {words_found} words found.')

	@property
	def weights_matrix(self):
		return self._weights_matrix

	def get_embedding(self):
		embedding = nn.Embedding.from_pretrained(torch.FloatTensor(self.weights_matrix))
		return deepcopy(embedding)

	@property
	def max_sequence_length(self):
		return self._max_sequence_length_without_start_end + 2 * (self._vocabulary.use_start_end)

	@property
	def max_sequence_length_without_start_end(self):
		return self._max_sequence_length_without_start_end

	@property
	def vocabulary_size(self):
		return self._vocabulary.size

	def get_word_index(self, word):
		return self._vocabulary[word]

	def encode(self, texts, echo=1):
		"""
		:type text: str
		:rtype: np.ndarray
		"""
		bar = ProgressBar(total=len(texts)); progress = -1
		lists = []
		for progress, text in enumerate(texts):
			text = self._vocabulary.clean_string(text)
			if progress/1000==progress//1000 and echo:
				bar.show(
					amount=progress,
					text=f'WE encoding sentence: "{shorten_string(s=text, max_length=20)}"'
				)
			lists.append(self._vocabulary.encode_sentence(sentence=text, max_length=self.max_sequence_length))
		if echo: bar.show(amount=progress+1, text=f'WE encoding done!"')
		return np.array(lists)




