from collections import Counter
import six
UNKNOWN_TOKEN = '<UNKNOWN>'
START_TOKEN = '<START>'
END_TOKEN = '<END>'
MASK_TOKEN = '<MASK>'

from ..string import remove_punctuations_for_one_string

class Vocabulary(object):

	def __init__(
			self, use_unknowns=False, unknown_token=UNKNOWN_TOKEN, use_mask=False, mask_token=MASK_TOKEN,
			use_start_end=False, start_token=START_TOKEN, end_token=END_TOKEN,
			case_sensitive=False, remove_punctuation=True
	):

		"""
		Args:
			use_unknowns (bool): The vocabulary will output UNK tokens for out of
				vocabulary items.
				[default=False]
			unknown_token (str): The token used for unknown tokens.
				If `use_unknowns` is True, this will be added to the vocabulary.
				[default='<UNK>']
			use_mask (bool): The vocabulary will reserve the 0th index for a mask token.
				This is used to handle variable lengths in sequence models.
				[default=False]
			mask_token (str): The token used for the mask.
				Note: mostly a placeholder; it's unlikely the token will be seen.
				[default='<MASK>']
			use_start_end (bool): The vocabulary will reserve indices for two tokens
				that represent the start and end of a sequence.
				[default=False]
			start_token: The token used to indicate the start of a sequence.
				If `use_start_end` is True, this will be added to the vocabulary.
				[default='<START>']
			end_token: The token used to indicate the end of a sequence
				 If `use_start_end` is True, this will be added to the vocabulary.
				 [default='<END>']
		"""

		self._mapping = {}  # str -> int
		self._flip = {}  # int -> str;
		self._counts = Counter()  # int -> int; count occurrences
		self._forced_unknowns = set()  # force tokens to unk (e.g. if < 5 occurrences)
		self._i = 0
		self._frozen = False
		self._frequency_threshold = -1
		self._case_sensitive = case_sensitive
		self._remove_punctuation = remove_punctuation

		# mask token for use in masked recurrent networks
		# usually need to be the 0th index
		self.use_mask = use_mask
		self.mask_token = mask_token
		if self.use_mask:
			self.add(self.mask_token)

		# unk token for out of vocabulary tokens
		self.use_unknowns = use_unknowns
		self.unknown_token = unknown_token
		if self.use_unknowns:
			self.add(self.unknown_token)

		# start token for sequence models
		self.use_start_end = use_start_end
		self.start_token = start_token
		self.end_token = end_token
		if self.use_start_end:
			self.add(self.start_token)
			self.add(self.end_token)

	def iterkeys(self):
		for k in self._mapping.keys():
			if k == self.unknown_token or k == self.mask_token:
				continue
			else:
				yield k

	def keys(self):
		return list(self.iterkeys())

	def iteritems(self):
		for key, value in self._mapping.items():
			if key == self.unknown_token or key == self.mask_token:
				continue
			yield key, value

	@property
	def items(self):
		"""
		:rtype: list[tuple(str, int)]
		"""
		return list(self.iteritems())

	@property
	def size(self):
		return len(self._mapping)

	def values(self):
		return [value for _, value in self.iteritems()]

	def __getitem__(self, k):
		if self._frozen:
			if k in self._mapping:
				out_index = self._mapping[k]
			elif self.use_unknowns:
				out_index = self.unknown_index
			else:  # case: frozen, don't want unknowns, raise exception
				raise Exception("Vocabulary is frozen. " +  "Key '{}' not found.".format(k))
			if out_index in self._forced_unknowns:
				out_index = self.unknown_index
		elif k in self._mapping:  # case: normal
			out_index = self._mapping[k]
			self._counts[out_index] += 1
		else:
			out_index = self._mapping[k] = self._i
			self._i += 1
			self._flip[out_index] = k
			self._counts[out_index] = 1

		return out_index

	def add(self, k):
		return self.__getitem__(k)

	def add_many(self, x):
		return [self.add(k) for k in x]

	def lookup(self, i):
		try:
			return self._flip[i]
		except KeyError:
			raise Exception("Key {} not in Vocabulary".format(i))

	def lookup_many(self, x):
		for k in x:
			yield self.lookup(k)

	def map(self, sequence):
		if self.use_start_end:
			yield self.start_index

		for item in sequence:
			yield self[item]

		if self.use_start_end:
			yield self.end_index

	def freeze(self, use_unknowns=False, frequency_cutoff=-1):
		self.use_unknowns = use_unknowns
		self._frequency_cutoff = frequency_cutoff

		if use_unknowns and self.unknown_token not in self:
			self.add(self.unknown_token)

		if self._frequency_cutoff > 0:
			for token, count in self._counts.items():
				if count < self._frequency_cutoff:
					self._forced_unknowns.add(token)

		self._frozen = True

	def unfreeze(self):
		self._frozen = False

	def get_counts(self):
		return {self._flip[i]: count for i, count in self._counts.items()}

	def get_count(self, token=None, index=None):
		if token is None and index is None:
			return None
		elif token is not None and index is not None:
			print("Cannot do two things at once; choose one")
		elif token is not None:
			return self._counts[self[token]]
		elif index is not None:
			return self._counts[index]
		else:
			raise Exception("impossible condition")

	@property
	def unknown_index(self):
		if self.unknown_token not in self:
			return None
		return self._mapping[self.unknown_token]

	@property
	def mask_index(self):
		if self.mask_token not in self:
			return None
		return self._mapping[self.mask_token]

	@property
	def start_index(self):
		if self.start_token not in self:
			return None
		return self._mapping[self.start_token]

	@property
	def end_index(self):
		if self.end_token not in self:
			return None
		return self._mapping[self.end_token]

	def __contains__(self, k):
		return k in self._mapping

	def __len__(self):
		return len(self._mapping)

	def __repr__(self):
		return "<Vocabulary(size={},frozen={})>".format(len(self), self._frozen)


	def get_serializable_contents(self):
		"""
		Creats a dict containing the necessary information to recreate this instance
		"""
		config = {"_mapping": self._mapping,
				  "_flip": self._flip,
				  "_frozen": self._frozen,
				  "_i": self._i,
				  "_counts": list(self._counts.items()),
				  "_frequency_threshold": self._frequency_threshold,
				  "use_unknowns": self.use_unknowns,
				  "unknown_token": self.unknown_token,
				  "use_mask": self.use_mask,
				  "mask_token": self.mask_token,
				  "use_start_end": self.use_start_end,
				  "start_token": self.start_token,
				  "end_token": self.end_token}
		return config

	@classmethod
	def deserialize_from_contents(cls, content):
		"""
		Recreate a Vocabulary instance; expect same dict as output in `serialize`
		"""
		try:
			_mapping = content.pop("_mapping")
			_flip = content.pop("_flip")
			_i = content.pop("_i")
			_frozen = content.pop("_frozen")
			_counts = content.pop("_counts")
			_frequency_threshold = content.pop("_frequency_threshold")
		except KeyError:
			raise Exception("unable to deserialize vocabulary")
		if isinstance(list(_flip.keys())[0], six.string_types):
			_flip = {int(k): v for k, v in _flip.items()}
		out = cls(**content)
		out._mapping = _mapping
		out._flip = _flip
		out._i = _i
		out._counts = Counter(dict(_counts))
		out._frequency_threshold = _frequency_threshold

		if _frozen:
			out.freeze(out.use_unknowns)

		return out

	def clean_string(self, s):
		if not isinstance(s, str): s= ''
		if self._remove_punctuation:
			s = remove_punctuations_for_one_string(
				string=s, replacement=' ', special_punctuations_replacement=''
			)
		if not self._case_sensitive: s = s.lower()
		return s

	def encode_sentence(self, sentence, max_length):
		clean_string = self.clean_string(sentence)
		words = clean_string.split(' ')
		result = list(self.map(words))
		if len(result) < max_length:
			result += [self.mask_index] * (max_length - len(result))
		else:
			result = result[:max_length]
		return result


