from .Vocabulary import Vocabulary, START_TOKEN, END_TOKEN
from pandas import Series
import numpy as np
import json

class Vectorizer:
	def __init__(self, vocabulary, max_sequence_length):
		"""
		:type vocabulary: Vocabulary
		:type max_sequence_length: int
		"""
		self._vocabulary = vocabulary
		self._max_sequence_length = max_sequence_length

	@property
	def vocabulary(self):
		"""
		:rtype: Vocabulary
		"""
		return self._vocabulary

	@property
	def max_sequence_length(self):
		"""
		:rtype: int
		"""
		return self._max_sequence_length

	def save(self, path):
		vector_dict = {
			'vocabulary': self.vocabulary.get_serializable_contents(),
			'max_sequence_length':self.max_sequence_length
		}
		with open(path, 'w') as file:
			json.dump(vector_dict, file)

	@classmethod
	def load(cls, path):
		with open(path, 'r') as file:
			vector_dict = json.load(file)
		vector_dict['vocabulary'] = Vocabulary.deserialize_from_contents(vector_dict['vocabulary'])
		return cls(vocabulary=vector_dict['vocabulary'], max_sequence_length=vector_dict['max_sequence_length'])

	@classmethod
	def fit(
			cls, x, use_unknowns=True, use_start_end=True, use_mask=True,
			start_token=START_TOKEN, end_token=END_TOKEN
	):
		"""
		:type x: Series
		"""
		vocabulary = Vocabulary(
			use_unknowns=use_unknowns, use_start_end=use_start_end, use_mask=use_mask,
			start_token=start_token, end_token=end_token
		)
		max_sequence_length = 0
		for text in x:
			split_text = text.split(" ")
			vocabulary.add_many(split_text)
			if len(split_text) > max_sequence_length: max_sequence_length=len(split_text)
		max_sequence_length += 2
		return cls(vocabulary=vocabulary, max_sequence_length=max_sequence_length)

	def transform(self, x):
		"""
		:type x: Series
		"""
		length = len(x)
		words = np.zeros((length, self.max_sequence_length), dtype=np.int64)
		for index, row in x.iteritems():
			converted = list(self.vocabulary.map(row.split(' ')))
			words[index, :len(converted)] = converted
		return words

	@classmethod
	def fit_transform(
			cls, x, use_unknowns=True, use_start_end=True, use_mask=True,
			start_token=START_TOKEN, end_token=END_TOKEN
	):
		"""
		:type x: Series
		:rtype tuple(Vectorizer, Series)
		"""
		vectorizer = cls.fit(
			x=x, use_unknowns=use_unknowns, use_start_end=use_start_end, use_mask=use_mask,
			start_token=start_token, end_token=end_token
		)
		return vectorizer, vectorizer.transform(x=x)

