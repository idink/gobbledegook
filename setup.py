from setuptools import setup, find_packages

setup(
	name='gobbledegook',
	version='0.2',
	description='language tools',
	url='',
	author='Idin',
	author_email='d@idin.net',
	license='GNU AGPLv3',
	classifiers=[
		'Development Status :: 3 - Alpha',
		'Intended Audience :: Developers',
		'Programming Language :: Python :: 3 :: Only',
		'Programming Language :: Python :: 3.6',
		'Programming Language :: Python :: 3.7',
		'Topic :: Software Development :: Libraries :: Python Modules'
	],
	packages=find_packages(exclude=["jupyter_tests", ".idea", ".git"]),
	install_requires=['pandas', 'torch', 'cleanco', 'editdistance', 'slytherin'],
	python_requires='~=3.6',
	zip_safe=False
)
